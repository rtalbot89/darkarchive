package EPrints::Plugin::Screen::EPrint::DarkArchive;

@ISA = ('EPrints::Plugin::Screen::EPrint::Move');

use strict;

sub new
{
    my ($class, %params) = @_;

    my $self = $class->SUPER::new(%params);
    $self->{actions} = [qw/ move_dark /];

    $self->{appears} = [
                        {
                         place    => "eprint_editor_actions",
                         action   => "move_dark",
                         position => 1900,
                        },
                       ];

    return $self;
} ## end sub new

sub allow_move_dark
{
    my ($self) = @_;

    return 0 unless $self->could_obtain_eprint_lock;
    return $self->allow("eprint/move_dark");
} ## end sub allow_move_dark

sub action_move_dark
{
    my ($self) = @_;

    my $ok = $self->{processor}->{eprint}->move_to_dark;

    $self->add_result_message($ok);
} ## end sub action_move_dark

# Add method to package

package EPrints::DataObj::EPrint;

use strict;

sub move_to_dark
{
    my ($self) = @_;

    my $success = $self->_transfer("dark");

    if ($success)
    {
        # supported but deprecated. use eprint_status_change instead.
        if ($self->{session}
            ->get_repository->can_call("update_submitted_eprint"))
        {
            $self->{session}
              ->get_repository->call("update_submitted_eprint", $self);
            $self->commit;
        } ## end if ($self->{session}->get_repository...)
    } ## end if ($success)

    return ($success);
} ## end sub move_to_dark

# Override item list rendering to include the filter for dark
package EPrints::Plugin::Screen::Items;

use strict;

sub render_items
{
    my ($self, $list) = @_;

    my $session   = $self->{session};
    my $user      = $session->current_user;
    my $chunk     = $session->make_doc_fragment;
    my $imagesurl = $session->current_url(path => "static", "style/images");
    my $ds        = $session->dataset("eprint");

    my $pref    = $self->{id} . "/eprint_status";
    my %filters = @{
        $session->current_user->preference($pref)
          || [
              inbox    => 1,
              buffer   => 1,
              archive  => 1,
              deletion => 1,
              dark     => 1,
             ]
    };

    my $filter_div = $session->make_element("div", class => "ep_items_filters");
    foreach my $f (qw/ inbox buffer archive deletion dark /)
    {
        my $url = URI->new($session->current_url());
        my %q   = $self->hidden_bits;
        $q{"set_show_$f"} = !$filters{$f};
        $url->query_form(%q);
        my $link = $session->render_link($url);
        if ($filters{$f})
        {
            $link->appendChild(
                               $session->make_element(
                                          "img",
                                          src => "$imagesurl/checkbox_tick.png",
                                          alt => "Showing"
                               )
                              );
        } ## end if ($filters{$f})
        else
        {
            $link->appendChild(
                               $session->make_element(
                                         "img",
                                         src => "$imagesurl/checkbox_empty.png",
                                         alt => "Not showing"
                               )
                              );
        } ## end else [ if ($filters{$f}) ]
        $link->appendChild($session->make_text(" "));
        $link->appendChild(
                     $session->html_phrase("eprint_fieldopt_eprint_status_$f"));
        $filter_div->appendChild($link);
        $filter_div->appendChild($session->make_text(". "));
    } ## end foreach my $f (qw/ inbox buffer archive deletion dark /...)

    my $columns = $session->current_user->get_value("items_fields");
    @$columns = grep { $ds->has_field($_) } @$columns;
    if (!EPrints::Utils::is_set($columns))
    {
        $columns = ["eprintid", "type", "eprint_status", "lastmod"];
        $session->current_user->set_value("items_fields", $columns);
        $session->current_user->commit;
    } ## end if (!EPrints::Utils::is_set...)

    my $len = scalar @{$columns};

    my $final_row = undef;
    if ($len > 1)
    {
        $final_row = $session->make_element("tr");
        my $imagesurl = $session->config("rel_path") . "/style/images";
        for (my $i = 0 ; $i < $len ; ++$i)
        {
            my $col = $columns->[$i];

            # Column headings
            my $td = $session->make_element("td", class => "ep_columns_alter");
            $final_row->appendChild($td);

            my $acts_table =
              $session->make_element(
                                     "table",
                                     cellpadding => 0,
                                     cellspacing => 0,
                                     border      => 0,
                                     width       => "100%"
                                    );
            my $acts_row = $session->make_element("tr");
            my $acts_td1 =
              $session->make_element("td", align => "left", width => "14");
            my $acts_td2 =
              $session->make_element("td", align => "center", width => "100%");
            my $acts_td3 =
              $session->make_element("td", align => "right", width => "14");
            $acts_table->appendChild($acts_row);
            $acts_row->appendChild($acts_td1);
            $acts_row->appendChild($acts_td2);
            $acts_row->appendChild($acts_td3);
            $td->appendChild($acts_table);

            if ($i != 0)
            {
                my $form_l = $session->render_form("post");
                $form_l->appendChild(
                              $session->render_hidden_field("screen", "Items"));
                $form_l->appendChild(
                                    $session->render_hidden_field("colid", $i));
                $form_l->appendChild(
                                     $session->make_element(
                                                   "input",
                                                   type  => "image",
                                                   value => "Move Left",
                                                   title => "Move Left",
                                                   src => "$imagesurl/left.png",
                                                   alt => "<",
                                                   name => "_action_col_left"
                                     )
                                    );
                $acts_td1->appendChild($form_l);
            } ## end if ($i != 0)
            else
            {
                $acts_td1->appendChild(
                                       $session->make_element(
                                                 "img",
                                                 src => "$imagesurl/noicon.png",
                                                 alt => ""
                                       )
                                      );
            } ## end else [ if ($i != 0) ]

            my $msg     = $self->phrase("remove_column_confirm");
            my $form_rm = $session->render_form("post");
            $form_rm->appendChild(
                              $session->render_hidden_field("screen", "Items"));
            $form_rm->appendChild($session->render_hidden_field("colid", $i));
            $form_rm->appendChild(
                $session->make_element(
                    "input",
                    type  => "image",
                    value => "Remove Column",
                    title => "Remove Column",
                    src   => "$imagesurl/delete.png",
                    alt   => "X",
                    onclick =>
                      "if( window.event ) { window.event.cancelBubble = true; } return confirm( "
                      . EPrints::Utils::js_string($msg) . ");",
                    name => "_action_remove_col"
                )
            );
            $acts_td2->appendChild($form_rm);

            if ($i != $len - 1)
            {
                my $form_r = $session->render_form("post");
                $form_r->appendChild(
                              $session->render_hidden_field("screen", "Items"));
                $form_r->appendChild(
                                    $session->render_hidden_field("colid", $i));
                $form_r->appendChild(
                                     $session->make_element(
                                                  "input",
                                                  type  => "image",
                                                  value => "Move Right",
                                                  title => "Move Right",
                                                  src => "$imagesurl/right.png",
                                                  alt => ">",
                                                  name => "_action_col_right"
                                     )
                                    );
                $acts_td3->appendChild($form_r);
            } ## end if ($i != $len - 1)
            else
            {
                $acts_td3->appendChild(
                                       $session->make_element(
                                                 "img",
                                                 src => "$imagesurl/noicon.png",
                                                 alt => ""
                                       )
                                      );
            } ## end else [ if ($i != $len - 1) ]
        } ## end for (my $i = 0 ; $i < $len...)
        my $td = $session->make_element("td",
                             class => "ep_columns_alter ep_columns_alter_last");
        $final_row->appendChild($td);
    } ## end if ($len > 1)

    # Paginate list
    my %opts = (
        params => {screen => "Items",},
        columns       => [@{$columns}, undef],
        above_results => $filter_div,
        render_result => sub {
            my ($session, $e, $info) = @_;

            my $class = "row_" . ($info->{row} % 2 ? "b" : "a");
            if ($e->is_locked)
            {
                $class .= " ep_columns_row_locked";
                my $my_lock =
                  ($e->get_value("edit_lock_user") ==
                    $session->current_user->get_id);
                if ($my_lock)
                {
                    $class .= " ep_columns_row_locked_mine";
                }
                else
                {
                    $class .= " ep_columns_row_locked_other";
                }
            } ## end if ($e->is_locked)

            my $tr = $session->make_element("tr", class => $class);

            my $status = $e->get_value("eprint_status");

            my $first = 1;
            for (@$columns)
            {
                my $td =
                  $session->make_element("td",
                              class => "ep_columns_cell ep_columns_cell_$status"
                                . ($first ? " ep_columns_cell_first" : "")
                                . " ep_columns_cell_$_");
                $first = 0;
                $tr->appendChild($td);
                $td->appendChild($e->render_value($_));
            } ## end for (@$columns)

            $self->{processor}->{eprint}   = $e;
            $self->{processor}->{eprintid} = $e->get_id;
            my $td =
              $session->make_element(
                                "td",
                                class => "ep_columns_cell ep_columns_cell_last",
                                align => "left"
              );
            $tr->appendChild($td);
            $td->appendChild(
                             $self->render_action_list_icons(
                                  "eprint_item_actions",
                                  {'eprintid' => $self->{processor}->{eprintid}}
                             )
                            );
            delete $self->{processor}->{eprint};

            ++$info->{row};

            return $tr;
        },
        rows_after => $final_row,
               );
    $chunk->appendChild(
                        EPrints::Paginate::Columns->paginate_list(
                                               $session, "_buffer", $list, %opts
                                                                 )
                       );

    # Add form
    my $div = $session->make_element("div", class => "ep_columns_add");
    my $form_add = $session->render_form("post");
    $form_add->appendChild($session->render_hidden_field("screen", "Items"));

    my $colcurr = {};
    foreach (@$columns) { $colcurr->{$_} = 1; }
    my $fieldnames = {};
    foreach my $field ($ds->get_fields)
    {
        next unless $field->get_property("show_in_fieldlist");
        next if $colcurr->{$field->get_name};
        my $name = EPrints::Utils::tree_to_utf8($field->render_name($session));
        my $parent = $field->get_property("parent_name");
        if (defined $parent)
        {
            my $pfield = $ds->get_field($parent);
            $name =
              EPrints::Utils::tree_to_utf8($pfield->render_name($session))
              . ": $name";
        } ## end if (defined $parent)
        $fieldnames->{$field->get_name} = $name;
    } ## end foreach my $field ($ds->get_fields...)

    my @tags =
      sort { $fieldnames->{$a} cmp $fieldnames->{$b} } keys %$fieldnames;

    $form_add->appendChild(
                           $session->render_option_list(
                                                        name     => 'col',
                                                        height   => 1,
                                                        multiple => 0,
                                                        'values' => \@tags,
                                                        labels   => $fieldnames
                                                       )
                          );

    $form_add->appendChild(
                           $session->render_button(
                                               class => "ep_form_action_button",
                                               name  => "_action_add_col",
                                               value => $self->phrase("add")
                           )
                          );
    $div->appendChild($form_add);
    $chunk->appendChild($div);

    # End of Add form

    return $chunk;
} ## end sub render_items

sub get_filters
{
	my( $self ) = @_;

	my $pref = $self->{id}."/eprint_status";
	my $user = $self->{session}->current_user;
	my @f = @{$user->preference( $pref ) || []};
	if( !scalar @f )
	{
		@f = ( inbox=>1, buffer=>1, archive=>1, deletion=>1, dark=>1 );
	}

	foreach my $i (0..$#f)
	{
		next if $i % 2;
		my $filter = $f[$i];
		my $v = $self->{session}->param( "set_show_$filter" );
		if( defined $v )
		{
			$f[$i+1] = $v;
			$user->set_preference( $pref, \@f );
			$user->commit;
			last;
		}
	}	

	my @l = map { $f[$_] } grep { $_ % 2 == 0 && $f[$_+1] } 0..$#f;

	return (
		{ meta_fields => [qw( eprint_status )], value => "@l", match => "EQ", merge => "ANY" },
	);
}

1;
