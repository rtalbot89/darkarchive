$c->{plugins}{"Screen::EPrint::DarkArchive"}{params}{disable} = 0;

$c->{datasets}->{dark} = {
    sqlname => "eprint",
    virtual => 1,
    class   => "EPrints::DataObj::EPrint",
    confid  => "eprint",
    import  => 1,
    index   => 1,
    filters =>
      [{meta_fields => ['eprint_status'], value => 'dark', describe => 0}],
    dataset_id_field => "eprint_status",
    datestamp        => "lastmod",
    columns          => [qw( eprintid type status_changed userid )]
    ,    # default columns for Review screen
                         };

$c->add_dataset_field(
                      "eprint",
                      {
                       name     => "eprint_status",
                       type     => "set",
                       required => 1,
                       options  => [qw( inbox buffer archive deletion dark )],
                       replace_core => 1
                      },
                      reuse => 1
                     );

# Add this role and give to admins and editors
$c->{roles}->{"move-dark"} = [
    "eprint/inbox/move_dark:editor",
    "eprint/buffer/move_dark:editor",
    "eprint/archive/move_dark:editor",
    "eprint/deletion/move_dark:editor",

    "eprint/dark/move_buffer:editor",
    "eprint/dark/move_archive:editor",
    "eprint/dark/move_deletion:editor",
    "eprint/dark/derive_version:editor",
    "eprint/dark/use_as_template:editor",
    "eprint/dark/remove:editor",

    "eprint/dark/view:editor",
    "eprint/dark/export:editor",
    "eprint/dark/summary:editor",
    "eprint/dark/export:editor",
    "eprint/dark/details:editor",
    "eprint/dark/history:editor",
    "eprint/dark/messages:editor",
    "eprint/dark/edit:editor",

    "eprint/dark/view",
    "eprint/dark/export",
    "eprint/dark/details",
    "eprint/dark/history",

    "eprint/dark/request_removal",
                             ];

push(@{$c->{user_roles}->{editor}}, "move-dark");
push(@{$c->{user_roles}->{admin}},  "move-dark");

# Dark archive additions to where things appear
$c->{plugins}->{"Screen::EPrint::Move"}->{actions}->{move_deletion}->{appears}
  ->{eprint_actions_bar_dark} = 100;
$c->{plugins}->{"Screen::EPrint::Remove"}->{appears}
  ->{eprint_actions_bar_dark} = 1000;
$c->{plugins}->{"Screen::EPrint::Move"}->{actions}->{move_buffer}->{appears}
  ->{eprint_actions_bar_dark} = 100;

